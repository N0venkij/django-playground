FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /composeexample
WORKDIR /composeexample
COPY requirements.txt /composeexample/
RUN pip install -r requirements.txt
COPY . /composeexample/
